$(function(){
	$('.js-refresh-quote').click(randomQoute);

	$('.js-sub-text-check').click(function(){
		var parent = $(this).parent();
		if ($(this).is(':checked')) {
			$('.b-textarea__second-text').show();
			
			parent.addClass('is-added');
			parent.find('.sub-text-toggle').text('Remove secondary text');
			var val = $('.js-sub-text').val();

			window.myText2 = new fabric.Text(val, {
				fontFamily: myText2FontFamily,
				fontSize: myText2FontSize,
				fontStyle: myTextFontStyle,
				fontWeight: myText2FontWeight,
				fill: myText2FontColor,
				left: 40,
				top: 150
			});
			mainCanvas.add(myText2);
		} else {
			parent.removeClass('is-added');
			parent.find('.sub-text-toggle').text('Add secondary text');
			mainCanvas.remove(myText2);
			window.myText2 = null;
			$('.b-textarea__second-text').hide();
		}
	});

	$(".js-upload-logo").on("change", function (e) {
		var files = e.target.files || e.dataTransfer.files;
		var file = files[0];
		var parent = $(this).parent();
		// display an image
		if (file.type.indexOf("image") === 0) {
			var reader = new FileReader();
			reader.onload = function(e) {
				// file.name 
				// e.target.result
				var imgElement;
				console.log('eq');
				if(!Logo){
					console.log('if');
					$('.js-logo').append('<img src="'+ e.target.result +'" />');
					$('.b-textarea__logo').fadeIn();
					parent.find('span').text('Change logo');
					console.log(Logo);
					imgElement = $('.js-logo').find('img').get(0);
					window.Logo = new fabric.Image(imgElement, {
					  left: mainCanvas.width / 2,
					  top: mainCanvas.height / 2,
					});

					mainCanvas.add(Logo);
					console.log(Logo);
					//mainCanvas.bringToFront(Logo);
					setTimeout(function(){ Logo.bringToFront(); }, 0);
					console.log(Logo);
				} else {
					$('.js-logo').find('img').remove();
					$('.js-logo').append('<img src="'+ e.target.result +'" />');
					console.log('else');
					Logo.remove();

					imgElement = $('.js-logo').find('img').get(0);
					window.Logo = new fabric.Image(imgElement, {
					  left: mainCanvas.width / 2,
					  top: mainCanvas.height / 2,
					});
					mainCanvas.add(Logo);
					//mainCanvas.bringToFront(Logo);
					//Logo.bringToFront();
					setTimeout(function(){ Logo.bringToFront(); }, 0);
				}				
			};
			reader.readAsDataURL(file);
		}
	});

	$('.js-logo-remove').click(function(){
		$('.js-logo').find('img').remove();
		$('.b-textarea__logo').fadeOut();
		$('.b-textarea__add-logo').find('span').text('Add a logo');
		Logo.remove();
		Logo = null;
	});

	$(".js-upload-background").on("change", function (e) {
		e.stopPropagation();
		e.preventDefault();
		var files = e.target.files || e.dataTransfer.files;
		var file = files[0];

		// display an image
		if (file.type.indexOf("image") === 0) {
			var reader = new FileReader();
			reader.onload = function(e) {
				// file.name 
				// e.target.result
				$('.b-images__item_upload').after('<li class="b-images__item"><img class="js-change-background" src="'+ e.target.result +'" /></li>');
				setTimeout(function(){
					$('.b-images__item_upload').next().find('.js-change-background').click();
				}, 0);
			};
			reader.readAsDataURL(file);
		}
	});

	$('.b-checkbox-single').click(function (e) {
		$('.js-bg-contrast').click();
	});

	$('.js-bg-contrast').click(function (e) {
		e.stopPropagation();
		if($('.js-bg-contrast:checked').length > 0) {
		 	background.filters.push(new fabric.Image.filters.Tint({
		 		color: '#000000',
		 		opacity: 0.2
		 	}));
		 	TintNumber = background.filters.length - 1;
	  		background.applyFilters(mainCanvas.renderAll.bind(mainCanvas));
		 	mainCanvas.add(background);
		} else {
			if (TintNumber !== null && TintNumber > -1) {
				background.filters.splice(TintNumber, 1);
				background.applyFilters(mainCanvas.renderAll.bind(mainCanvas));
			 	mainCanvas.add(background);
			 	TintNumber = null;
		 	}
		}
		mainCanvas.bringToFront(myText);
	});

	$('.js-background-type').click(function (e) {
		e.stopPropagation();
		console.log('called: ' + Math.random());
		var val = $(this).val();
		if(val == 'normal'){
			if (bgTypeNumber !== null && bgTypeNumber > -1) {
				background.filters.splice(bgTypeNumber, 1);
				background.applyFilters(mainCanvas.renderAll.bind(mainCanvas));
			 	mainCanvas.add(background);
			 	bgTypeNumber = null;
			 	bgTypeLastFilter = null;
		 	}
		}
		else if(val == 'blur' && val !== bgTypeLastFilter){
			if (bgTypeNumber !== null && bgTypeNumber > -1) {
				background.filters.splice(bgTypeNumber, 1);
				background.applyFilters(mainCanvas.renderAll.bind(mainCanvas));
			 	mainCanvas.add(background);
		 	}

		 	background.filters.push(new fabric.Image.filters.Convolute({
	  			matrix: [1/84, 1/84, 1/84, 2/84, 1/84, 1/84, 1/84,
                                        1/84, 1/84, 2/84, 2/84, 2/84, 1/84, 1/84,
                                        1/84, 2/84, 3/84, 3/84, 3/84, 2/84, 1/84,
                                        2/84, 2/84, 3/84, 4/84, 3/84, 2/84, 2/84,
                                        1/84, 2/84, 3/84, 3/84, 3/84, 1/84, 1/84,
                                        1/84, 1/84, 2/84, 2/84, 2/84, 1/84, 1/84,
                                        1/84, 1/84, 1/84, 2/84, 1/84, 1/84, 1/84
                                ]
            }));
		 	bgTypeNumber = background.filters.length - 1;
	  		background.applyFilters(mainCanvas.renderAll.bind(mainCanvas));
		 	mainCanvas.add(background);
		 	bgTypeLastFilter = 'blur';
		}
		else if(val == 'black' && val !== bgTypeLastFilter) {
			if (bgTypeNumber !== null && bgTypeNumber > -1) {
				background.filters.splice(bgTypeNumber, 1);
				background.applyFilters(mainCanvas.renderAll.bind(mainCanvas));
			 	mainCanvas.add(background);
		 	}

		 	background.filters.push(new fabric.Image.filters.Grayscale());
		 	bgTypeNumber = background.filters.length - 1;
	  		background.applyFilters(mainCanvas.renderAll.bind(mainCanvas));
		 	mainCanvas.add(background);
		 	bgTypeLastFilter = 'black';
		}
		mainCanvas.bringToFront(myText);
		/*mainCanvas.sendToBack(window.background);
		console.log(88);
		setTimeout(function(background){
			window.background.sendToBack();
			console.log(99)
		}, 1000);*/
	});

	$('.js-font-family').click(function () {
		console.log($(this).data('value'));
		
		myTextFontFamily = $(this).data('value');
		myText.fontFamily = myTextFontFamily;

		mainCanvas.renderAll();

		$('.js-font-family-current').text(myTextFontFamily).css('font-family', myTextFontFamily);
	});

	$('.js-font-size').click(function () {
		console.log($(this).data('value'));

		myTextFontSize = $(this).data('value');
		myText.fontSize = myTextFontSize;
		mainCanvas.renderAll();

		var size = (myTextFontSize == 20) ? 'Small' : (myTextFontSize == 22) ? 'Medium' : (myTextFontSize == 24) ? 'Large' : (myTextFontSize == 26)  ? 'X-Large' : 20;

		$('.js-font-size-current').text(size);
	});

	$('.js-font-color').click(function () {
		console.log($(this).data('value'));

		myTextFontColor = $(this).data('value');
		myText.fill = myTextFontColor;
		mainCanvas.renderAll();

		$('.js-font-color-current').css('background', myTextFontColor);
	});

	$('.js-font-weight').click(function (e){
		e.stopPropagation();
		console.log($(this).val());

		myTextFontWeight = $(this).val();
		myText.fontWeight = myTextFontWeight;
		mainCanvas.renderAll();
	});

	$('.js-font-style').change(function (e) {
		e.stopPropagation();
		
		myTextFontStyle = ($('.js-font-style:checked').length > 0) ? 'italic' : 'normal';
		console.log(myTextFontStyle);
		myText.fontStyle = myTextFontStyle;
		mainCanvas.renderAll();
	});

	$(document).on('click', '.js-change-background', function(){
		$('.b-images__item.ss-check').removeClass('ss-check');

		var cloneImg = $(this).clone().css({
			width: 'auto',
			height: 'auto',
			display: 'none'
		}).addClass('js-clone-img');
		
		$('.b-actions__box_download').append(cloneImg);

		imgElement = $('.js-clone-img').get(0);

		$(this).parent().addClass('ss-check');

		window.background.remove();

		window.background = new fabric.Image(imgElement, {
			hasRotatingPoint: false,
			lockRotation: true,
			hasControls: false,
			width: 512,
		 	height: 256
		});

		background.filters.push(new fabric.Image.filters.Tint({
	 		color: '#000000',
	 		opacity: 0.2
	 	}));
	 	TintNumber = background.filters.length - 1;
	 	background.applyFilters(mainCanvas.renderAll.bind(mainCanvas));

		mainCanvas.add(background);
		background.sendToBack();

		$('.js-clone-img').remove();		
	});

	$('.js-save-result').click(function(e){
		e.preventDefault();
		var png = mainCanvas.toDataURL();

		if(Modernizr.adownload){
			 var link = document.createElement("a");
		     link.download = "pablo.png";
		     link.href = png;

		     var theEvent = document.createEvent("MouseEvent");
		     theEvent.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
		     link.dispatchEvent(theEvent);
		} else {
			window.open(png);
		}
	});

});
