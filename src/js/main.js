var qoutes = [
	'\u201CIt does not matter how slowly you go as long as you do not stop.\u201D\r\n \r\n\u2013 Confucius',
	'\u201CAn unexamined life is not worth living.\u201D\r\n \r\n\u2013 Socrates',
	'\u201CLife isn\u2019t about getting and having, it\u2019s about giving and being.\u201D\r\n \r\n\u2013 Kevin Kruse',
	'\u201CThink like a wise man but communicate in the language of the people.\u201D\r\n \r\n- William Butler Yeats',
	'\u201CThe best thing I did was choose the right heroes.\u201D\r\n \r\n- Warren Buffett'
];
var TintNumber = null;
var bgTypeNumber = null;
var bgTypeLastFilter = null;
var background = null;
var lastQoute = null;
var myText = null;
var myText2 = null;
var Logo = null;
var myTextFontFamily = 'Merriweather';
var myText2FontFamily = 'Merriweather';
var myTextFontSize = 20;
var myText2FontSize = 20;
var myTextFontColor = '#fff';
var myText2FontColor = '#fff';
var myTextFontStyle = 'normal';
var myText2FontStyle  = 'normal';
var myTextFontWeight = 400;
var myText2FontWeight  = 400;


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomPic() {
	var length = $('.b-images__item').find('img').length;
	return getRandomInt(0, length-1);
}

function drawBackground(){
	var rand = getRandomPic();
	imgElement = $('.b-images__item').find('img').get(rand);

	$('.b-images__item').find('img').eq(rand).parent().addClass('ss-check');

	fabric.Image.fromURL(imgElement.src, function (bg) {
		window.background = bg;
		bg.hasRotatingPoint = false;
		bg.lockRotation = true;
		bg.hasControls = false;
		bg.width = 512;
	 	bg.height = 256;
	 	bg.filters.push(new fabric.Image.filters.Tint({
	 		color: '#000000',
	 		opacity: 0.2
	 	}));
	 	TintNumber = bg.filters.length - 1;
  		bg.applyFilters(mainCanvas.renderAll.bind(mainCanvas));
	 	mainCanvas.add(bg);
	 	randomQoute();
	});
}

function randomQoute() {
	var rand = getRandomInt(0, qoutes.length - 1);
	var randQoute = qoutes[rand];
	if (lastQoute === randQoute) randomQoute();
	else {
		lastQoute = randQoute;
		$('.js-qoute').val(randQoute);
		if(!myText){
			window.myText = new fabric.Text(randQoute, {
				fontFamily: myTextFontFamily,
				fontSize: myTextFontSize,
				fontStyle: myTextFontStyle,
				fontWeight: myTextFontWeight,
				fill: myTextFontColor,
				left: 10,
				top: 10
			});
			mainCanvas.add(myText);
		} else {
			mainCanvas.remove(myText);
			window.myText = new fabric.Text(randQoute, {
				fontFamily: myTextFontFamily,
				fontSize: myTextFontSize,
				fontStyle: myTextFontStyle,
				fontWeight: myTextFontWeight,
				fill: myTextFontColor,
				left: 10,
				top: 10
			});
			mainCanvas.add(myText);
		}
		$('.js-font-family-current').text(myTextFontFamily).css('font-family', myTextFontFamily);
		var size = (myTextFontSize == 20) ? 'Small' : (myTextFontSize == 22) ? 'Medium' : (myTextFontSize == 24) ? 'Large' : (myTextFontSize == 26)  ? 'X-Large' : 20;

		$('.js-font-size-current').text(size);
		$('.js-font-color-current').css('background', myTextFontColor);
	}
}

$(window).ready(function(){
	$('.js-loading-div').fadeOut();
});

$(function(){
	window.mainCanvas = new fabric.Canvas('mainCanvas', { backgroundColor : "#909493",width: '512',height: '256' });
	
	drawBackground();
});
