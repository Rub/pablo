# Pablo

Design engaging images for your social media posts in under 30 seconds

### Version
0.0.1

### Tech

Pablo uses a number of open source projects to work properly:

* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework
* [Gulp] - the streaming build system
* [jQuery] - for DOM manipulations

### Installation

You need Gulp installed globally:

```sh
$ npm i -g gulp
```

```sh
$ git clone [git-repo-url] Pablo
$ cd Pablo
$ npm i
$ gulp build --prod
```

### Development

Want to contribute? Great!

Pablo uses Gulp + Webpack for fast developing.
Make a change in your file and instantanously see your updates!

Open your favorite Terminal and run these commands.

First Tab:
```sh
$ gulp
```

Second Tab:
```sh
$ node app
```

License
----

MIT

**Free Software, Hell Yeah!**

[node.js]:http://nodejs.org
[jQuery]:http://jquery.com
[express]:http://expressjs.com
[Gulp]:http://gulpjs.com
