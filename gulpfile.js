'use strict';
var gulp = require('gulp'),
	watch = require('gulp-watch'),
	runSequence = require('run-sequence'),
	clean = require('gulp-clean'),
	changed = require('gulp-changed'),
	server = require('gulp-develop-server'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload,
	jade = require('gulp-jade'),
	concat = require('gulp-concat'),
	minifyCss = require('gulp-minify-css'),
	uglify = require('gulp-uglify'),
	autoprefixer = require('gulp-autoprefixer'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	jshint = require('gulp-jshint');

gulp.task('serve', function () {
	server.listen( { path: './bin/www' } );
});

// livereload browser on client app changes
gulp.task('livereload', ['serve'], function(){
	browserSync( {
	   proxy: "localhost:3000"
	});
});

gulp.task('img-min', function () {
	gulp.src('public/img/*', {read: false}).pipe(clean());
	gulp.src('src/img/**/*')
    .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        use: [pngquant()]
    }))
    .pipe(gulp.dest('./public/img'));
});

gulp.task('js', function () {
	gulp.src('public/js/*', {read: false}).pipe(clean());
	gulp.src('src/js/**/*.js')
	.pipe(jshint())
  	.pipe(jshint.reporter('default'))
	.pipe(uglify())
	.pipe(concat('all.js'))
	.pipe(gulp.dest('./public/js'));
});

gulp.task('css', function () {
	gulp.src('public/css/*', {read: false}).pipe(clean());
	gulp.src('src/css/**/*.css')
	.pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))
	.pipe(minifyCss())
	.pipe(concat('all.css'))
	.pipe(gulp.dest('./public/css'));
});

gulp.task('jade', function () {
	gulp.src('views/*', {read: false}).pipe(clean());
	gulp.src('src/templates/**/*.jade')
	.pipe(jade({ pretty: true }))
	.pipe(gulp.dest('views/'));
});

gulp.task('del', function () {
	return gulp.src(['public', 'views'], {read: false}).pipe(clean());
});

gulp.task('copy', function (){
	var DEST = 'public/';
	return gulp.src([
					'src/vendor/jquery/dist/jquery.min.js',
					'src/vendor/fabric/dist/fabric.min.js',
					'src/fonts/**/*',
					'src/favicon.ico'
					], { 
			base: 'src'
		})
        .pipe(changed(DEST))
        .pipe(gulp.dest(DEST));
});

gulp.task('watch', ['livereload'], function () {
	watch(['src/templates/**/*.jade'], function(){
		gulp.run('jade');
		console.log('restarting browsers');
		reload();
	});
	watch(['src/css/**/*.css'], function(){
		gulp.run('css');
		console.log('restarting browsers');
		reload();
	});
	watch(['src/js/**/*.js'], function(){
		gulp.run('js');
		console.log('restarting browsers');
		reload();
	});
	watch(['src/img/**/*'], function(){
		gulp.run('img-min');
		console.log('restarting browsers');
		reload();
	});
	watch(['./app.js', 'bin/**/*'], function(){
		server.restart();
		reload();
		console.log('restarting server');
	});
	console.log('watch started');
});

gulp.task('build', ['del'], function(cb) {
    return runSequence('copy', 'img-min', 'jade', 'css', 'js', cb);
});

gulp.task('default', ['build'], function() {
    return gulp.start('watch');
});
